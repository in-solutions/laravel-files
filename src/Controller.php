<?php

namespace Insolutions\Files;
 
use Illuminate\Http\Request;

// INS/Files/File as FileEntry
use Insolutions\Files\File as FileEntry; 
use Illuminate\Http\File as File;

use Illuminate\Support\Facades\Storage;

use Insolutions\Items\Service as ItemsService;
use Insolutions\Items\Tag;

use Auth;

class Controller extends \App\Http\Controllers\Controller
{
    public function list(Request $r) {
        return response()->json(
            FileEntry::paginate($r->perPage ?: 50)
        );
    }

    public function suggestedTags(Request $r, $file_id) {
        return response()->json(
            Tag::orderBy('updated_at', 'desc')->paginate($r->perPage ?: 10)
        );
    }

    public function tags(Request $r, $file_id, $tag_id = null) {
    	$model = FileEntry::findOrFail($file_id);

    	if ($r->isMethod('get')) {
            return response()->json(
                ItemsService::getTags($model)
            );
        } 
        else if ($r->isMethod('post')) 
    	{
    		$tag = Tag::findOrFail($tag_id);
    		ItemsService::addTag($model, $tag);
    	} 
    	else if ($r->isMethod('delete'))
    	{
    		$tag = Tag::findOrFail($tag_id);
    		ItemsService::removeTag($model, $tag);
    	} 
    	else if ($r->isMethod('put')) 
    	{
    		$tags = [];
    		foreach ($r->tags as $tag_id) {
    			$tag = Tag::findOrFail($tag_id);
				$tags[] = $tag;
    		}
    		ItemsService::syncTags($model, $tags);
    	}
    }

	 public function serve($file_name) {
	 	$file_entry = FileEntry::where(['file_name' => $file_name])->firstOrFail();
    	$path = storage_path('app/' . $file_entry->getFilePath());

	    return response()
	    	->file($path, ["Content-Type" => $file_entry->mime_type]);
    }

    public function upload(Request $r) {
		$file = $r->file('uploadedFile');
		$file_name = uniqid(true) . "." . $file->getClientOriginalExtension();
		$path = 'tmp';

		$file->storeAs($path, $file_name);

		$file_entry = new FileEntry();
		$file_entry->path = $path;
		$file_entry->file_name = $file_name;
		$file_entry->mime_type = $file->getMimeType();
		$file_entry->title = $file->getClientOriginalName();
		$file_entry->is_temporary = true;
		$file_entry->save();

    	return response()->json($file_entry);
    }

	public function persist(Request $r, $file_id) {
		$fileEntry = FileEntry::findOrFail($file_id);
		$fileEntry->persist();

		return response()->json($fileEntry->fresh());
	}

}