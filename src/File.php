<?php

namespace Insolutions\Files;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Storage;
use Insolutions\Items\ItemTrait;

class File extends Model
{
    use SoftDeletes, ItemTrait;

    protected $table = 't_file';

    protected $fillable = ['title', 'path', 'file_name', 'mime_type'];
    protected $hidden = ['deleted_at', 'created_at'];
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];
   
    protected $appends = ['url'];

    protected $casts = [
        'is_temporary' => 'boolean'
    ];

    public function getUrlAttribute() {
    	return route('file_path', ['file_name' => $this->file_name] );
    }

    public function getFilePath() {
    	return $this->path . '/' . $this->file_name;
    }
    
    public function persist() {
        if ($this->is_temporary) {
            $this->is_temporary = false;
            $this->moveTo("public");
        }
    }
    
    public function moveTo($path) {
        Storage::disk()->move($this->getFilePath(), $path . '/' . $this->file_name);
        $this->path = $path;
        $this->save();
    }

    public function deleteFileFromDisk() {
        return Storage::disk()->delete($this->getFilePath());
    }

    public static function deleteOldTemporaryFiles($daysToKeep = 7) {
        $filesToDelete = File::where('is_temporary', 1)
            ->where('created_at', '<=', new Carbon("-{$daysToKeep} days"))
            ->get();

        /** @var File $fileEntry */
        foreach ($filesToDelete as $fileEntry) {
            if ($fileEntry->deleteFileFromDisk()) {
                $fileEntry->forceDelete();
            }
        }
    }
}
