<?php

namespace Insolutions\Files;

class ServiceProvider extends \Illuminate\Support\ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {        
        
        $this->publishes([
            __DIR__.'/../db' => base_path('database/sql/insolutions/files'),
//            __DIR__.'/../app/Listeners' => app_path('Listeners'),
//            __DIR__.'/views' => base_path('resources/views/ins/ecommerce'),        
//            __DIR__.'/assets' => public_path('ins/ecommerce'),
        ]);
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        include __DIR__.'/routes.php';
        $this->app->make('Insolutions\Files\Controller');
    }
}
