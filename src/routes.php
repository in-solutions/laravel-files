<?php

use Insolutions\Files\Controller;

Route::group(['prefix' => 'files'], function () {
	
	Route::get('/file', Controller::class . '@list');
	Route::post('/upload', Controller::class . '@upload');

	Route::match(['post', 'put'],'/file/{file_id}/persist', Controller::class . '@persist');

	Route::get('/storage/{file_name}', Controller::class . '@serve')->name('file_path');

	Route::get('/file/{file_id}/tags/suggested', Controller::class . '@suggestedTags');

	Route::match(['post', 'delete'], '/file/{file_id}/tag/{tag_id}', Controller::class . '@tags');
	Route::match(['put','get'], '/file/{file_id}/tag', Controller::class . '@tags');
});